Docker images for different versions of PHP.

# Tags
- latest
- 5.3
- 5.5
- 5.6
- 7.0
- 7.1
- 7.2

# Usage

    docker run --rm -v "$(pwd):/app" -w /app gustav83/php:7.0 php -l index.php

# From source

    docker build --tag php7.0 php7.0/
    docker run --rm -v "$(pwd):/app" -w /app php7.0 php -l index.php

